package com.desafio.resources;

import com.desafio.models.DigitoUnico;
import com.desafio.repository.DigitoUnicoRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(value = "API de Digito Único")
@RestController
@RequestMapping("/digitounico")
public class DigitoUnicoResource {

    @Autowired
    private DigitoUnicoRepository digUniRepository;

    @ApiOperation(value ="Busca lista de todos os regidtros de dígito único")
    @GetMapping
    public List<DigitoUnico> listar(){

        return digUniRepository.findAll();
    }
    @ApiOperation(value ="Busca digito único por id")
    @GetMapping("/{idUsuario}")
    public ResponseEntity<Object> buscaPorId(@PathVariable Long id){
        Optional<DigitoUnico> digitoUnico = digUniRepository.findById(id);
        if (!digitoUnico.isPresent()){
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<>(digitoUnico.get(), HttpStatus.OK);
    }

    @ApiOperation(value ="Salva um objeto digito único")
    @PostMapping
    public int cadastraDigitoUnico (@RequestBody @Valid DigitoUnico digitoUnico){
        int numero = digitoUnico.getNumero();
        int rep = digitoUnico.getRepeticoes();
        int resultado = digitoUnico.CalculaDigitoUnico(numero, rep);
        digitoUnico.setResultado(resultado);
        digUniRepository.save(digitoUnico);
        return resultado;
    }

}
