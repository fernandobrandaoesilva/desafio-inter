package com.desafio.resources;

import com.desafio.models.Usuario;
import com.desafio.repository.UsuarioRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(value = "API Rest Usuários")
@RestController
@RequestMapping("/usuario")
public class UsuarioResource {

    @Autowired
    private UsuarioRepository ur;

    @ApiOperation(value ="Retorna uma lista de todos os usuários")
    @GetMapping
    public List<Usuario> listar(){
        return ur.findAll();
    }

    @ApiOperation(value ="Salva um usuários")
    @PostMapping
    public Usuario cadastraUsuario (@RequestBody @Valid Usuario usuario){

        return ur.save(usuario);
    }

    @ApiOperation(value ="Deleta um usuário")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void>  remover (@PathVariable Long id){
        if (!ur.findById(id).isPresent()){
            return ResponseEntity.notFound().build();
        }
        Usuario usuario = ur.getOne(id);
        ur.delete(usuario);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value ="Atualiza um usuário")
    @PutMapping("/{id}")
    public ResponseEntity<Usuario>  atualizar(@PathVariable Long id, @Valid @RequestBody Usuario usuarioAtual){
        Optional<Usuario> UsuarioExistente = ur.findById(id);
        if(UsuarioExistente.isPresent()){
            Usuario usuario = UsuarioExistente.get();
            usuario.setNome(usuarioAtual.getNome());
            usuario.setEmail(usuarioAtual.getEmail());
            ur.save(usuario);
            return ResponseEntity.ok(usuario);
        }
        else
            return ResponseEntity.notFound().build();
    }

}
