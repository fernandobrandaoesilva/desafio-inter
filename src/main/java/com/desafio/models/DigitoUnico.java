package com.desafio.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "DIGITOUNICO")
public class DigitoUnico {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long idUsuario;

    private int numero;

    private int repeticoes;

    private int resultado;


    public DigitoUnico(){

    }

    public DigitoUnico(Long idUsuario, int numero, int repeticoes, int resultado) {
        this.idUsuario = idUsuario;
        this.numero = numero;
        this.repeticoes = repeticoes;
        this.resultado = resultado;
    }

    public DigitoUnico(int numero, int repeticoes, int resultado) {
        this.numero = numero;
        this.repeticoes = repeticoes;
        this.resultado = resultado;
    }


    public int getResultado(){
        return resultado;
    }

    public void setResultado(int resultado){
        this.resultado = resultado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getRepeticoes() {
        return repeticoes;
    }

    public void setRepeticoes(int repeticoes) {
        this.repeticoes = repeticoes;
    }


    public int CalculaDigitoUnico(int numero, int repeticoes) {
        String tamanho = Integer.toString(numero);
        String resultado = ConcatenaString(numero, repeticoes);
        int digitoUnico = Integer.parseInt(resultado);

        if (tamanho.length() == 1) {
            return numero;
        } else {
            int soma = 0;
            //123 -> dividirmos por 10, o resultado será 12, e sobra = 3
            //(123 / 10) = 12 -> (123 % 10) = 3
            //(12  / 10) = 1  -> (12  % 10) = 2
            //(1   / 10) = 0  -> (1   % 10) = 1
            boolean flag = false;
            String somaString;
            while(digitoUnico>0 && !flag) {
                // 1) obter o módulo do número digitado
                //int modulo = num % 10;
                // 2) incrementar o módulo a nossa variavel soma
                soma += (digitoUnico % 10);
                // 3) dividir o número por 10 e atribuirmos a nossa varial 'num'
                digitoUnico /= 10;//num = num / 10;
                //System.out.println("O valor de soma é: " + soma);
                if(digitoUnico==0){
                    somaString = Integer.toString(soma);
                    if(somaString.length()==1){
                        flag = true;
                    }
                    else{
                        digitoUnico = soma;
                        soma = 0;

                    }
                }
            }

            return soma;
        }

    }


    //Funcao que concatena as String de acordo com o numero de repeticoes
    private String ConcatenaString(int numero, int repeticoes){
        String stringConcatenada = "";
        for (int i = 0; i < repeticoes; i++) {
            stringConcatenada += Integer.toString(numero);

        }
        return stringConcatenada;
    }
}
