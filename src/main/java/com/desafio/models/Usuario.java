package com.desafio.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity(name = "USUARIO")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String nome;

    @NotBlank
    private String email;

//    private List<DigitoUnico> listaDigitoUnico;



//    public List<DigitoUnico> getListaDigitoUnico() {
//        return listaDigitoUnico;
//    }

//    public void setListaDigitoUnico(List<DigitoUnico> listaDigitoUnico) {
//        this.listaDigitoUnico = listaDigitoUnico;
//    }

    public Usuario(@NotBlank String nome, @NotBlank String email) {
        this.nome = nome;
        this.email = email;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getNome() {

        return nome;
    }

    public void setNome(String nome) {

        this.nome = nome;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }
}