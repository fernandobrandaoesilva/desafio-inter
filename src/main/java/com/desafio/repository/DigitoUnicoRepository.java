package com.desafio.repository;

import com.desafio.models.DigitoUnico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico,Long> {
}
