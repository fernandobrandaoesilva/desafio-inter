package com.desafio;

import com.desafio.models.Usuario;
import com.desafio.repository.UsuarioRepository;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@RunWith(SpringRunner.class)
public class UsuarioRepositoryTest {

    @Autowired
    private UsuarioRepository usuarioRepository;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void DeveCriarUsuario(){
        Usuario usuario = new Usuario("Usuario teste","testes@teste.com");
        this.usuarioRepository.save(usuario);
        Assertions.assertThat(usuario.getId()).isNotNull();
        Assertions.assertThat(usuario.getNome()).isEqualTo("Usuario teste");
        Assertions.assertThat(usuario.getEmail()).isEqualTo("testes@teste.com");
    }

    @Test
    public void DeveDeletarUsuario(){
        Usuario usuario = new Usuario("Usuario teste delete","testesdel@teste.com");
        this.usuarioRepository.save(usuario);
        usuarioRepository.delete(usuario);;
        Assertions.assertThat(usuarioRepository.findById(usuario.getId())).isEmpty();
    }

    @Test
    public void DeveAtualizarUsuario(){
        Usuario usuario = new Usuario("Usuario teste","testes@teste.com");
        this.usuarioRepository.save(usuario);
        usuario.setNome("Usuario atualizado");
        usuario.setEmail("emailatualizado@teste.com");
        Assertions.assertThat(usuario.getId()).isNotNull();
        Assertions.assertThat(usuario.getNome()).isEqualTo("Usuario atualizado");
        Assertions.assertThat(usuario.getEmail()).isEqualTo("emailatualizado@teste.com");
    }
}
