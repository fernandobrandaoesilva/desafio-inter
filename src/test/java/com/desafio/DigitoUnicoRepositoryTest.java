package com.desafio;

import com.desafio.models.DigitoUnico;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class DigitoUnicoRepositoryTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void DeveCalcularDigitoUnico(){
        DigitoUnico digitoUnico = new DigitoUnico();
        int resultado = digitoUnico.CalculaDigitoUnico(444,2);
        Assert.assertEquals(resultado,6);
    }

}
