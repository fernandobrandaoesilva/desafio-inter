# Objetivo

Projeto criado com o objetivo de realizar um desafio técnico para criar API de cadastro de usuários e cálculo de dígito único.

# O que foi utilizado

*  Maven
*  Spring Boot
*  Banco de Dados em memória (H2)
*  Swagger
*  Junit
*  Java 8


# Empacotando o projeto

***mvn package***

gera arquivo exemplo.jar no diretório target. O arquivo gerado não é executável. Um arquivo jar é um arquivo no formato zip. Você pode verificar o conteúde deste arquivo ao executar o comando jar vft exemplo.jar.

***mvn package -P executavel-dir***

gera exemplo-dir.jar, executável, mas dependente do diretório jars, também criado no diretório target. Para executar basta o comando java -jar target/exemplo-dir.jar. Observe que se o diretório jars for removido, então este comando falha. Por último, o diretório jars deve ser depositado no mesmo diretório do arquivo exemplo.jar.

***mvn package -P executavel-unico***

gera jar executável correspondente ao aplicativo a ser executado via linha de comandos, em um único arquivo, target/exemplo-unico.jar, suficiente para ser transferido e executado. Para executá-lo basta o comando java -jar target/exemplo-unico.jar.

***mvn package -P api***

gera jar executável juntamente com todas as dependências reunidas em um único arquivo, target/api.jar. Este arquivo jar pode ser transferido para outro diretório ou máquina e ser executado pelo comando java -jar target/api.jar. A execução e exemplos de chamadas são fornecidos na seção seguinte.



# Limpar, compilar, executar testes de unidade e cobertura

***mvn clean***


remove diretório target

***mvn compile***


compila o projeto, deposita resultados no diretório target

***mvn test***


executa todos os testes do projeto. Para executar apenas parte dos testes, por exemplo, aqueles contidos em uma dada classe execute mvn -Dtest=NomeDaClasseTest test. Observe que o sufixo do nome da classe de teste é Test (padrão recomendado). Para executar um único teste mvn -Dtest=NomeDaClasseTest#nomeDoMetodo test.

***mvn verify -P cobertura***

 
executa testes de unidade e produz relatório de cobertura em target/site/jacoco/index.html além de verificar se limite mínimo de cobertura, conforme configurado, é satisfeito.


# Banco de Dados (H2)
Foi utilizado o H2, que executa em memória. Foi escolhido pela simplicidade. 
